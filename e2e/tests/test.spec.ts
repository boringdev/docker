import { test, expect, Page } from "@playwright/test";

test.beforeEach(async ({ page }) => {
  await page.goto("http://web:5000/");
});

test.describe("test", () => {
  test("should see home page", async ({ page }) => {
    const locator = await page.locator("body");
    expect(await locator.textContent()).toContain("Hello World");
  });
});
